# main bb file: yocto-poky/meta/recipes-support/sqlite/sqlite3_3.35.0.bb

# version in openEuler
PV = "3.36.0"

# openEuler repo name
OPENEULER_REPO_NAME = "sqlite"

SRC_URI[sha256sum] = "bd90c3eb96bee996206b83be7065c9ce19aef38c3f4fb53073ada0d0b69bbce3"
